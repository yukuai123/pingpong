import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Frame from "../layout/Frame"
import Home from "../views/Home"
import Winner from "../views/Winner"
import PK from "../views/PK"
import Login from "../views/Login"

export default (
    <Router>
        <Frame>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/winner-list" component={Winner} />
                <Route path="/pk" component={PK} />
                <Route path={["/login", "/register"]} component={Login} />
            </Switch>
        </Frame>
    </Router>
)
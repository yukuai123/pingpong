export function createTypeAction(type) {
    return {
        type,
        payload: null,
    }
}
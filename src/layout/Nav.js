import React from 'react'
import { Menu } from 'antd'
import { Link } from 'react-router-dom'
const { Item } = Menu

export default function Nav() {
    return (
        <Menu
            mode="horizontal"
            style={{ lineHeight: '64px' }}>
            <Item>
                <Link to="/">
                    积分榜
                </Link>
            </Item>
            <Item>
                <Link to="winner-list">
                    冠军榜
                </Link>
            </Item>
            <Item>
                <Link to="/pk">
                    PK榜
                </Link>
            </Item>
            <Item>
                <Link to="/login">
                    管理
                </Link>
            </Item>
        </Menu>
    )
}
import React from "react";
import ReactDOM from "react-dom";
import initStore from "./redux/createStore";
import { Provider } from "react-redux";
import routes from "./routes"
import 'antd/dist/antd.css';
import "./styles.css";

//初始化Redux状态容器
const store = initStore();
function App() {
  return (
    <div className="App">
      <Provider store={store}>
        {routes}
      </Provider>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);

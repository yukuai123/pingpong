# PK榜设计

整个PK榜分为三个组件来完成,同样地,PK榜页面本身是view组件，其余三个是component组件

1. PKInput
用于输入需要搜索的运动员姓名,表现为两个输入框分别输入homeName, awayName。
需要完善的功能: **错误提示** **加载提示** **显示区域的自动处理**

2.PKInfoList
展示运动员总览信息

3.PKPlayersHistory
展示所有运动员的历史记录

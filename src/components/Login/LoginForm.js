import React, { Component } from "react"
import { Form, Input, Button, Checkbox } from "antd"
import { Link } from "react-router-dom"
const { Item } = Form

class LoginForm extends Component {
    compareToFirstPassword = (rule, value, callback) => {
        const {form} = this.props
        if (value && value != form.getFieldValue('password')) {
            callback('两次密码不一致')
        } else {
            callback()
        }
    }

    handleSubmit = e => {
        e.preventDefault()
        //注册逻辑
        if (this.isRegister) {
            
        //登录逻辑
        } else {

        }
    }

    handleLogin = e => {

    }

    handleRegister = e => {

    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { isRegister } = this.props
        return (
            <Form>
                <Item>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: '请输入用户名' }]
                    })(<Input placeholder="请输入用户名" size="large" />)}
                </Item>
                <Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: "请输入密码" }]
                    })(<Input.Password placeholder="请输入密码" size="large" type="password" />)}
                </Item>
                {
                    isRegister ?
                        <Item>
                            {getFieldDecorator('confrim', {
                                rules: [{ required: true, message: "请再次输入密码" }, {
                                    validator: this.compareToFirstPassword,
                                }]
                            })(<Input.Password placeholder="请再次输入密码" size="large" type="password" />)}
                        </Item> :
                        null
                }
                <Item>
                    {
                        !isRegister ?
                            <Checkbox>Remember me</Checkbox> :
                            null
                    }
                </Item>
                <Item>
                    <Button type="primary" block>
                        {!isRegister ? "注册" : "登录"}
                    </Button>
                </Item>
                <Item>
                    {
                        isRegister ?
                            <Link to="/login">去登录</Link> :
                            <Link to="/register">去注册</Link>
                    }

                </Item>
            </Form>
        )
    }
}

const WrappedLoginForm = Form.create({ name: "login_form" })(LoginForm)
export default WrappedLoginForm
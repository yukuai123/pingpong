import React from "react"
import {Row, Col} from 'antd'
import style from "./PKInfo.module.css"
export default function (props) {
    const { left, right, title = "" } = props;
    return (
        <Row className={style.block}>
            <Col span={10}>
                {left}
            </Col>
            <Col className={style.title} span={4}>
                {title}
            </Col>
            <Col span={10}>
                {right}
            </Col>
        </Row>
    )
}
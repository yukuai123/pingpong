const initialState = {
    homeName: "一飞",
    awayName: "东东",
}


const SET_NAME = 'set_name'

export function setName(key, value) {
    return {
        type: SET_NAME,
        payload: {
            key,
            value
        }
    }
}

export default function reducer(state = initialState, action) {
    const {type, payload} = action
    switch(type) {
        case SET_NAME:
            return {
                ...state,
                [`${payload.key}`]: payload.value,
            }
        default:
            return state
    }
}
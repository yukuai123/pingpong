import React, { Component } from 'react'
import { Input, Button, Form, Row } from 'antd'
import style from "./PKInput.module.css"

const { Item } = Form

export default class PKInput extends Component {

    constructor(props) {
        super(props)

        this.handleClick = this.handleClick.bind(this)
        this.handleHomeInput = this.handleHomeInput.bind(this)
        this.handleAwayInput = this.handleAwayInput.bind(this)
        this.state = {
            homeName: this.props.homeName,
            awayName: this.props.awayName,
        }
    }

    handleHomeInput(e) {
        this.setState({
            homeName: e.target.value,
        })
    }

    handleAwayInput(e) {
        this.setState({
            awayName: e.target.value,
        })
    }

    handleClick() {
        const { onClick } = this.props
        const { homeName, awayName } = this.state
        onClick(homeName, awayName)
    }

    render() {
        const { homeName, awayName } = this.props

        return (
            <Form>
                <Item>
                    <Input
                        size="large"
                        onChange={this.handleHomeInput}
                        defaultValue={homeName}
                        placeholder="请输入球员姓名" />
                </Item>

                <Item>
                    <Input
                        size="large"
                        onChange={this.handleAwayInput}
                        defaultValue={awayName}
                        placeholder="请输入球员姓名" />
                </Item>
                <Item>
                    <Button block type="primary" onClick={this.handleClick}>VS</Button>
                </Item>
            </Form >
        )
    }

}
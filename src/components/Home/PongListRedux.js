/**
 * 在此文件定义
 * 1. Home页面相关的reducer
 * 2. Home页面相关的action creator
 * 该文件是纯净的JS定义文件
 * 默认导出reducer
 * 导出action creator
 */

/**
 * action遵守FSA
 */
const LOAD_DATA = "load_data"
const LOAD_DATA_SUCCESS = "load_data_success"
const LOAD_DATA_ERROR = "load_data_error"
export function loadAllData(queryName = "") {
  return {
    types: [LOAD_DATA, LOAD_DATA_SUCCESS, LOAD_DATA_ERROR],
    url: `https://pping.goho.co/api/score/query?name=${queryName}&is_show=1`,
  };
}

const initState = {
  players: [],
  isLoading: false,
};

const handleMaps = {
  [`${LOAD_DATA}`]: actionLoadAllData,
  [`${LOAD_DATA_SUCCESS}`]: actionLoadAllDataSuccess,
};


export default function reducer(state = initState, action) {
  const handler = handleMaps[action.type];
  if (!!handler) {
    return handler(state, action);
  } else {
    return state;
  }
}

function actionLoadAllData(state, action) {
  return {
    ...state,
    isLoading: true,
  };
}

function actionLoadAllDataSuccess(state, action) {
  const { scoreList } = action.payload;

  const newScoreList = scoreList
    .sort((a, b) => b['score'] - a['score'])
    .map((v, i) => {
      return {
        ...v,
        id: i + 1
      } 
    })

  return {
    ...state,
    isLoading: false,
    players: newScoreList,
  }
}
import React, { Component } from "react"
import { Modal, Table } from "antd"
import WinnerIcon from "../shared/WinnerIcon"

class HistoryModal extends Component {
    static COLUMNS = [
        {
            title: '姓名',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '比赛',
            dataIndex: 'game_name',
            align: "left",
            key: 'game_name',
            render: (gameName, row) => {
                return row.is_winner ?
                    (
                        <div>
                            {gameName} <WinnerIcon />
                        </div>
                    ) : (
                        <span> {gameName}</span >
                    );
            }
        },
        {
            title: '积分',
            dataIndex: 'score',
            key: 'score',
        },
        {
            title: '变动情况',
            dataIndex: 'game_count',
            key: 'game_count',
        },
    ];

    render() {
        const { visible, onCancel, info } = this.props
        return (
            <Modal
                title="比赛记录"
                visible={visible}
                onCancel={onCancel}
                footer={null}>
                <Table
                    rowKey="id"
                    dataSource={info}
                    columns={HistoryModal.COLUMNS}
                    pagination={false}
                />
            </Modal>
        )
    }
}

export default HistoryModal
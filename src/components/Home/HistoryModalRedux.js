
const GET_PLAY_HISTORIC_RECORDS = "get_play_historic_records"
const LOAD_PLAY_HISTORIC_RECORDS = "load_play_historic_records"
const LOAD_PLAY_HISTORIC_RECORDS_SUCCESS = "load_play_historic_records_success"
const LOAD_PLAY_HISTORIC_RECORDS_ERROR = "load_play_historic_records_error"

//所有运动员的记录,缓存层
const playerHistoricRecords = new Map()

//action creator
export function loadPlayerHistory(name) {
    if (playerHistoricRecords.has(name)) {
        return {
            type: GET_PLAY_HISTORIC_RECORDS,
            payload: playerHistoricRecords.get(name),
        }
    } else {
        return {
            url: `https://pping.goho.co/api/score/history?name=${name}`,
            types: [LOAD_PLAY_HISTORIC_RECORDS, LOAD_PLAY_HISTORIC_RECORDS_SUCCESS, LOAD_PLAY_HISTORIC_RECORDS_ERROR],
        }
    }
}

//init state
const initState = {
    player: null,
}

//reducer
export default function reducer(state = initState, action) {
    switch (action.type) {
        case GET_PLAY_HISTORIC_RECORDS:
            return {
                ...state,
                player: action.payload
            }
        case LOAD_PLAY_HISTORIC_RECORDS_SUCCESS:
            const { payload } = action;

            const newPayload = payload
                .filter(v => !!v['game_name'])
                .map((v, id) => {
                    return {
                        id: id,
                        ...v,
                    }
                })

            if (newPayload.length > 0) {
                const name = newPayload[0]['name']
                playerHistoricRecords.set(name, newPayload)
            }

            return {
                ...state,
                player: newPayload,
            }
        case LOAD_PLAY_HISTORIC_RECORDS_ERROR:
            return state
        default:
            return state
    }
}
const initialState = {
    winners: [],
    isloading: false,
}

const LOAD_WINNER_LIST = "load_winner_list"
const LOAD_WINNER_LIST_SUCCESS = "load_winner_list_success"
const LOAD_WINNER_LIST_ERROR = "load_winner_list_error"

export function loadWinners(queryName = "") {
    return {
        types: [LOAD_WINNER_LIST, LOAD_WINNER_LIST_SUCCESS, LOAD_WINNER_LIST_ERROR],
        url: `http://localhost:8080/winner-list?name=${queryName}`,
    }
}


export default function reducer(state = initialState, action) {
    const { type, payload } = action
    switch (type) {
        case LOAD_WINNER_LIST:
            return {
                ...state,
                winners: [],
                isloading: true,
            }
        case LOAD_WINNER_LIST_SUCCESS:
            return {
                ...state,
                winners: payload,
                isloading: false,
            }
        case LOAD_WINNER_LIST_ERROR:
            return {
                ...state,
                isloading: false,
            }
        default:
            return state
    }
}